package com.joshafeinberg.contactreminder.adapters;

import android.content.Context;
import android.net.Uri;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.joshafeinberg.contactreminder.AddReminderFragment;
import com.joshafeinberg.contactreminder.R;
import com.joshafeinberg.contactreminder.models.Reminder;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class ContactExpandableAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private Map<String, List<Reminder.ReminderItem>> mContactReminders;
    private List<String> mContactList;

    public ContactExpandableAdapter(Context mContext, List<String> mContactList,
                                    Map<String, List<Reminder.ReminderItem>> mContactReminders) {
        this.mContext = mContext;
        this.mContactList = mContactList;
        this.mContactReminders = mContactReminders;
    }

    @Override
    public int getGroupCount() {
        return mContactList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (mContactList.size() <= groupPosition) {
            return 0;
        }
        return mContactReminders.get(mContactList.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mContactList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition)  {
        return mContactReminders.get(mContactList.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.contact_list_item, parent, false);
        }

        if (convertView == null) {
            return null;
        }

        String contactID = (String) getGroup(groupPosition);
        Reminder.ReminderItem reminderItem = mContactReminders.get(contactID).get(0);
        String lookupID = reminderItem.lookupID;
        AddReminderFragment.ContactDetails contactDetails =
                AddReminderFragment.getContactDetailsByID(mContext, contactID, lookupID);

        if (contactDetails.contactPhotoURI != null) {
            ((ImageView) convertView.findViewById(R.id.reminderContactImage)).setImageURI(Uri.parse(contactDetails.contactPhotoThumbnailURI));
        }

        ((TextView) convertView.findViewById(R.id.reminderContactName)).setText(contactDetails.contactName);



        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.reminder_list_item, parent, false);
        }

        if (convertView == null) {
            return null;
        }

        Reminder.ReminderItem reminderItem = mContactReminders.get(mContactList.get(groupPosition)).get(childPosition);

        ((TextView) convertView.findViewById(R.id.reminderText)).setText(reminderItem.reminder);

        if (reminderItem.dueDate > 0) {
            java.text.DateFormat dateFormat = DateFormat.getDateFormat(mContext);
            String dateString = dateFormat.format(new Date(reminderItem.dueDate));
            java.text.DateFormat timeFormat = DateFormat.getTimeFormat(mContext);
            String timeString = timeFormat.format(new Date(reminderItem.dueDate));

            ((TextView) convertView.findViewById(R.id.reminderDueDate)).setText(dateString + " " + timeString);
        } else {
            convertView.findViewById(R.id.reminderDueDate).setVisibility(View.GONE);
        }


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return true;
    }
}
