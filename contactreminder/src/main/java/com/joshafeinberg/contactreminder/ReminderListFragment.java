package com.joshafeinberg.contactreminder;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;


import com.joshafeinberg.contactreminder.adapters.ContactExpandableAdapter;
import com.joshafeinberg.contactreminder.databases.DatabaseHelper;
import com.joshafeinberg.contactreminder.models.Reminder;

public class ReminderListFragment extends Fragment implements ExpandableListView.OnChildClickListener {


    private Reminder mReminderClass;

    private ExpandableListView expandableListView;
    private ContactExpandableAdapter mAdapter;
    private ActionMode mActionMode;
    private Reminder.ReminderItem mSelectedReminderItem;
    private int mSelectedGroup;
    private boolean[] mExpandedGroups;
    private int mSelectedChild;


    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            MenuInflater inflater = actionMode.getMenuInflater();
            if (inflater == null) {
                return false;

            }
            inflater.inflate(R.menu.action_mode_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_deleteReminder:
                    Log.i("contactreminder", "Deleting Item => " + mSelectedReminderItem.reminderID);
                    DatabaseHelper databaseHelper = DatabaseHelper.getInstance(getActivity());
                    databaseHelper.deleteRemindersById(mSelectedReminderItem.reminderID);
                    updateListView();
                    for (int i = 0; i < mReminderClass.contactReminders.size(); ++i) {
                        mExpandedGroups[i] = false;
                    }
                    mActionMode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            mActionMode = null;
            mSelectedReminderItem = null;
            mSelectedChild = -1;
            expandableListView.clearChoices();
            expandableListView.requestLayout();
        }
    };


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ReminderListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mReminderClass = new Reminder(getActivity());
        if (savedInstanceState == null) {
            mSelectedGroup = -1;
            mSelectedChild = -1;
            mExpandedGroups = new boolean[mReminderClass.contactReminders.size()];
        } else {
            Log.i("contactreminder", "Loaded Previous");
            mSelectedGroup = savedInstanceState.getInt("selectedGroup", -1);
            mSelectedChild = savedInstanceState.getInt("selectedChild", -1);
            mExpandedGroups = savedInstanceState.getBooleanArray("expandedGroups");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_reminder_list_exp, container, false);

        expandableListView = (ExpandableListView) rootView;
        if (expandableListView == null) {
            return null;
        }


        mAdapter = new ContactExpandableAdapter(getActivity(), mReminderClass.contactList,
                mReminderClass.contactReminders);

        expandableListView.setAdapter(mAdapter);

        expandableListView.setChoiceMode(ExpandableListView.CHOICE_MODE_SINGLE);

        expandableListView.setOnChildClickListener(this);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                // TODO: need to fix this to just expanded groups incase you expand multiple
                mExpandedGroups[groupPosition] = true;
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                mExpandedGroups[groupPosition] = false;
                if (mSelectedGroup == groupPosition && mActionMode != null) {
                    mActionMode.finish();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateListView();
        for (int i = 0; i < mReminderClass.contactReminders.size(); ++i) {
            if (mExpandedGroups.length - 1 < i) {
                mExpandedGroups = new boolean[mReminderClass.contactReminders.size()];
            }
            if (mExpandedGroups[i]) {
                expandableListView.expandGroup(i);
            }
        }
        if (mSelectedChild != -1) {
            expandableListView.setSelectedChild(mSelectedGroup, mSelectedChild, true);
            onChildClick(expandableListView, expandableListView, mSelectedGroup, mSelectedChild, 0);
        }
    }

    private void updateListView() {
        mReminderClass = new Reminder(getActivity());
        mAdapter = new ContactExpandableAdapter(getActivity(), mReminderClass.contactList,
                mReminderClass.contactReminders);
        expandableListView.setAdapter(mAdapter);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("selectedGroup", mSelectedGroup);
        outState.putInt("selectedChild", mSelectedChild);
        outState.putBooleanArray("expandedGroups", mExpandedGroups);
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long l) {
        int index = parent.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, childPosition));
        parent.setItemChecked(index, !parent.isItemChecked(index));

        if (mActionMode != null && !parent.isItemChecked(index)) {
            mActionMode.finish();
            return false;
        }

        if (mActionMode == null) {
            mActionMode = getActivity().startActionMode(mActionModeCallback);
        }
        mSelectedReminderItem = (Reminder.ReminderItem) mAdapter.getChild(groupPosition, childPosition);
        mSelectedGroup = groupPosition;
        mSelectedChild = childPosition;
        v.setSelected(true);

        return true;
    }
}
