package com.joshafeinberg.contactreminder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;


public class AddReminderActivity extends FragmentActivity {

    public static String FRAGMENT_TAG = "AddReminderTag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_add);

        if (getActionBar() == null) {
            return;
        }
        // Show the Up button in the action bar.
        getActionBar().setDisplayHomeAsUpEnabled(true);

        AddReminderFragment addReminderFragment =
                (AddReminderFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);

        if (savedInstanceState == null && addReminderFragment == null) {
            AddReminderFragment fragment = new AddReminderFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.reminder_detail_container, fragment, FRAGMENT_TAG)
                    .commit();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpTo(this, new Intent(this, ReminderListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
