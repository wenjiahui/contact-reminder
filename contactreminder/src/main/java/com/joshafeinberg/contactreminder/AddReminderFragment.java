package com.joshafeinberg.contactreminder;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.joshafeinberg.contactreminder.databases.DatabaseHelper;
import com.joshafeinberg.contactreminder.services.NotifListenerService;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class AddReminderFragment extends Fragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private static final int CONTACT_PICKER_RESULT = 1001;
    public static final String CUSTOM_ACTION = "com.joshafeinberg.contactreminder.DUE_DATE_ALERT";

    private ContactDetails mContactDetails;
    private int mYear;
    private int mMonth;
    private int mDay;
    private boolean mTimeSet;
    private int mHour;
    private int mMinute;
    private long mTimeMilli;


    public AddReminderFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        if (savedInstanceState == null) {
            Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                    ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(contactPickerIntent, CONTACT_PICKER_RESULT);
        } else {
            String id = savedInstanceState.getString("contactID");
            String lookupID = savedInstanceState.getString("lookupID");
            mContactDetails = getContactDetailsByID(getActivity(), id, lookupID);

            mTimeSet = savedInstanceState.getBoolean("timeSet", false);
            if (mTimeSet) {
                mYear = savedInstanceState.getInt("year");
                mMonth = savedInstanceState.getInt("month");
                mDay = savedInstanceState.getInt("day");
                mHour = savedInstanceState.getInt("hour");
                mMinute = savedInstanceState.getInt("minute");
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_reminder, container, false);

        if (rootView == null) {
            return null;
        }


        final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, mYear, mMonth, mDay);

        Button reminderDueDate = (Button) rootView.findViewById(R.id.reminderDueDate);
        reminderDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTimeSet = false;
                datePickerDialog.show();
            }
        });

        Button cancelAddButton = (Button) rootView.findViewById(R.id.cancelAddButton);
        cancelAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        final EditText reminder = (EditText) rootView.findViewById(R.id.reminderEditText);
        Button addButton = (Button) rootView.findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (reminder.getText() != null && reminder.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), R.string.reminderemptyerror, Toast.LENGTH_SHORT).show();
                    return;
                }
                DatabaseHelper databaseHelper = DatabaseHelper.getInstance(getActivity());
                long reminderID = databaseHelper.addReminder(mContactDetails.contactID,
                                           mContactDetails.lookupID,
                                           reminder.getText().toString(),
                                           mTimeMilli);
                if (mTimeSet) {
                    addAlarmToManager(getActivity(), reminderID, mTimeMilli);
                }
                getActivity().onBackPressed();
            }
        });


        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showContactDetails(getView());
        if (mTimeSet) {
            setDueDateText();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mContactDetails != null) {
            outState.putString("contactID", mContactDetails.contactID);
            outState.putString("lookupID", mContactDetails.lookupID);
            if (mTimeSet) {
                outState.putInt("year", mYear);
                outState.putInt("month", mMonth);
                outState.putInt("day", mDay);
                outState.putBoolean("timeSet", mTimeSet);
                outState.putInt("hour", mHour);
                outState.putInt("minute", mMinute);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CONTACT_PICKER_RESULT:
                    Uri result = data.getData();
                    if (result == null) {
                        return;
                    }
                    // get the contact id from the Uri
                    List<String> results = result.getPathSegments();
                    assert results != null;
                    String lookupID = results.get(results.size() - 2);
                    String id = result.getLastPathSegment();
                    Log.i("contactreminder", "Lookup ID => " + lookupID);
                    Log.i("contactreminder", "Found ID => " + id);
                    mContactDetails = getContactDetailsByID(getActivity(), id, lookupID);
                    Log.i("contactreminder", "Found Name => " + mContactDetails.contactName);
                    Log.i("contactreminder", "Found URI => " + mContactDetails.contactPhotoURI);
                    showContactDetails(getView());
                    break;
            }

        } else {
            // gracefully handle failure
            Toast.makeText(getActivity(), R.string.contact_selection_error, Toast.LENGTH_LONG).show();
            NavUtils.navigateUpTo(getActivity(), new Intent(getActivity(), ReminderListActivity.class));
            Log.i("contactreminder", "Warning: activity result not ok");
        }
    }

    private void showContactDetails(View view) {
        if (mContactDetails != null && view != null) {
            if (mContactDetails.contactPhotoURI != null) {
                ((ImageView) view.findViewById(R.id.contactPhoto)).setImageURI(Uri.parse(mContactDetails.contactPhotoURI));
            }

            if (getActivity().getActionBar() != null) {
                getActivity().getActionBar().setTitle(mContactDetails.contactName);
            }

        }
    }


    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        mYear = year;
        mMonth = monthOfYear;
        mDay = dayOfMonth;

        // just in case a user cancels after setting date only
        setDueDateText();

        if (!mTimeSet) {
            mTimeSet = true;
            new TimePickerDialog(getActivity(), this, mHour, mMinute,
                    DateFormat.is24HourFormat(getActivity())).show();
        }
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
        mHour = hour;
        mMinute = minute;

        setDueDateText();
    }

    public void setDueDateText() {
        Calendar calendar = new GregorianCalendar(mYear, mMonth, mDay, mHour, mMinute);
        mTimeMilli = calendar.getTimeInMillis();
        java.text.DateFormat dateFormat = DateFormat.getDateFormat(getActivity());
        String dateString = dateFormat.format(new Date(mTimeMilli));
        java.text.DateFormat timeFormat = DateFormat.getTimeFormat(getActivity());
        String timeString = timeFormat.format(new Date(mTimeMilli));

        if (getView() != null) {
            Button reminderDueDate = (Button) getView().findViewById(R.id.reminderDueDate);
            reminderDueDate.setText(dateString + " " + timeString);
        }
    }

    /**
     * adds due date to alarm manager
     * @param context activity context
     * @param reminderID reminder database ID
     * @param alarmTime due date
     */
    public static void addAlarmToManager(Context context, long reminderID, long alarmTime) {
        Intent myIntent = new Intent(context, NotifListenerService.class);
        myIntent.putExtra("reminderID", reminderID);
        myIntent.setAction(CUSTOM_ACTION);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, (int) reminderID, myIntent,0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC, alarmTime, pendingIntent);
    }

    /**
     * Removes an alarm from the {@link android.app.AlarmManager}
     * @param context  activity context
     * @param reminderID  reminder database id
     */
    public static void removeAlarmFromManager(Context context, long reminderID) {
        Intent myIntent = new Intent(context, NotifListenerService.class);
        myIntent.putExtra("reminderID", reminderID);
        myIntent.setAction(CUSTOM_ACTION);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, (int) reminderID, myIntent,0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }

    public static class ContactDetails {
        public final String contactID;
        public final String lookupID;
        public final String contactName;
        public final String contactPhotoURI;
        public final String contactPhotoThumbnailURI;

        public ContactDetails(String contactID, String lookupID, String contactName, String contactPhotoURI, String contactPhotoThumbnailURI) {
            this.contactID = contactID;
            this.lookupID = lookupID;
            this.contactName = contactName;
            this.contactPhotoURI = contactPhotoURI;
            this.contactPhotoThumbnailURI = contactPhotoThumbnailURI;

        }
    }

    public static ContactDetails getContactDetailsByID(Context context, String contactID, String lookupID) {

        Uri uri = ContactsContract.Contacts.getLookupUri(Long.valueOf(contactID), lookupID);
        if (uri == null) {
            return null;
        }

        ContactDetails contactDetails = null;

        ContentResolver contentResolver = context.getContentResolver();
        Cursor contactLookup = contentResolver.query(uri, new String[] {BaseColumns._ID,
                        ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI,
                        ContactsContract.PhoneLookup.PHOTO_URI},
                null, null, null);

        try {
            if (contactLookup != null && contactLookup.getCount() > 0) {
                contactLookup.moveToNext();
                contactDetails = new ContactDetails(
                        contactLookup.getString(contactLookup.getColumnIndex(BaseColumns._ID)),
                        lookupID, contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME)),
                        contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.PhoneLookup.PHOTO_URI)),
                        contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI)));
            }
        } finally {
            if (contactLookup != null) {
                contactLookup.close();
            }
        }

        return contactDetails;
    }
}
