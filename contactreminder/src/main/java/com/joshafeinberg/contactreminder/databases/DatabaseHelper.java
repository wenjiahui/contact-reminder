package com.joshafeinberg.contactreminder.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.joshafeinberg.contactreminder.AddReminderFragment;
import com.joshafeinberg.contactreminder.models.Reminder;

import java.util.ArrayList;
import java.util.List;


public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "reminders.db";
    private static final int DATABASE_VERSION = 1;
    private static Context mContext;
    private static DatabaseHelper mDatabaseHelper;
    private static SQLiteDatabase mDatabase;

    /**
     * @param context activity context
     * @see android.database.sqlite.SQLiteOpenHelper
     */
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        DatabaseHelper.mContext = context;
    }

    public static DatabaseHelper getInstance(Context context) {
        if (mDatabaseHelper == null) {
            mDatabaseHelper = new DatabaseHelper(context);
        }
        return mDatabaseHelper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.i("contactreminder", "DatabaseHelper.onCreate");
        ReminderTable.onCreate(sqLiteDatabase);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        ReminderTable.onUpgrade(sqLiteDatabase, oldVersion, newVersion);
    }

    @Override
    public void close() {
        super.close();
        if (mDatabase != null) {
            mDatabase.close();
            mDatabase = null;
        }
    }

    public SQLiteDatabase getMyWritableDatabase() {
        if ((mDatabase == null) || (!mDatabase.isOpen())) {
            mDatabase = this.getWritableDatabase();
        }

        return mDatabase;
    }

    /**
     * adds a new reminder to the database
     * @param contactID contact id
     * @param lookupID  lookup key
     * @param reminder  reminder text
     * @param dueDate   due date (optional)
     * @return reminder id
     */
    public long addReminder(String contactID, String lookupID, String reminder, long dueDate) {
        SQLiteDatabase db = this.getMyWritableDatabase();
        if (db == null) {
            return -1;
        }

        ContentValues values = new ContentValues();
        values.put(ReminderTable.COLUMN_CONTACT, contactID);
        values.put(ReminderTable.COLUMN_LOOKUPCONTACT, lookupID);
        values.put(ReminderTable.COLUMN_REMINDER, reminder);
        values.put(ReminderTable.COLUMN_DUEDATE, dueDate / 1000L); // fix for milliseconds

        return db.insert(ReminderTable.TABLE_NAME, null, values);
    }

    /**
     * retrieves all reminders from database
     * @return {@link java.util.List} containing all reminders
     */
    public List<Reminder.ReminderItem> getAllReminders() {
        List<Reminder.ReminderItem> reminderList = new ArrayList<Reminder.ReminderItem>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + ReminderTable.TABLE_NAME + " ORDER BY " + ReminderTable.COLUMN_CONTACT;

        SQLiteDatabase db = this.getMyWritableDatabase();
        if (db == null) {
            return null;
        }

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Reminder.ReminderItem reminderItem = new Reminder.ReminderItem(cursor.getString(0),
                        cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getLong(4) * 1000);
                reminderList.add(reminderItem);
            } while (cursor.moveToNext());
        }

        db.close();

        return reminderList;
    }

    /**
     * Retrieves a list of reminders for a contact
     * @param contactID contact id
     * @return  {@link java.util.List} containing all reminders for a contact
     */
    public List<Reminder.ReminderItem> getRemindersForContact(String contactID) {
        List<Reminder.ReminderItem> reminderList = new ArrayList<Reminder.ReminderItem>();

        SQLiteDatabase db = this.getReadableDatabase();

        if (db == null) {
            return null;
        }

        Cursor cursor = db.query(ReminderTable.TABLE_NAME,
                new String[]{ReminderTable.COLUMN_ID, ReminderTable.COLUMN_CONTACT,
                        ReminderTable.COLUMN_LOOKUPCONTACT, ReminderTable.COLUMN_REMINDER,
                        ReminderTable.COLUMN_DUEDATE},
                ReminderTable.COLUMN_CONTACT + "=?",
                new String[]{contactID}, null, null, null, null
        );

        if (cursor.moveToFirst()) {
            do {
                Reminder.ReminderItem reminderItem = new Reminder.ReminderItem(cursor.getString(0),
                        cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getLong(4) * 1000);
                reminderList.add(reminderItem);
            } while (cursor.moveToNext());
        }

        db.close();

        return reminderList;
    }

    /**
     * returns a single reminder item by id
     * @param reminderID reminder database id
     * @return {@link com.joshafeinberg.contactreminder.models.Reminder.ReminderItem}
     */
    public Reminder.ReminderItem getReminderById(long reminderID) {
        SQLiteDatabase db = this.getMyWritableDatabase();
        if (db == null) {
            return null;
        }

        Cursor cursor = db.query(ReminderTable.TABLE_NAME,
                new String[] { ReminderTable.COLUMN_ID, ReminderTable.COLUMN_CONTACT,
                        ReminderTable.COLUMN_LOOKUPCONTACT, ReminderTable.COLUMN_REMINDER,
                        ReminderTable.COLUMN_DUEDATE},
                ReminderTable.COLUMN_ID + "=?",
                new String[] { Long.toString(reminderID)}, null, null, null, null);

        Reminder.ReminderItem reminderItem = null;
        if (cursor.moveToFirst()) {
            reminderItem = new Reminder.ReminderItem(cursor.getString(0), cursor.getString(1),
                    cursor.getString(2), cursor.getString(3), cursor.getLong(4) * 1000);
        }

        db.close();

        return reminderItem;
    }

    /**
     * deletes a users reminders
     * @param contactID contact id
     */
    public void deleteRemindersForContact(String contactID) {
        List<Reminder.ReminderItem> remindersForContact = getRemindersForContact(contactID);
        for (Reminder.ReminderItem reminderItem : remindersForContact) {
            AddReminderFragment.removeAlarmFromManager(mContext, Long.valueOf(reminderItem.reminderID));
        }

        SQLiteDatabase db = this.getMyWritableDatabase();
        if (db != null) {
            db.delete(ReminderTable.TABLE_NAME, ReminderTable.COLUMN_CONTACT + " = ?",
                    new String[] { contactID });
            //db.close();
        }
    }

    /**
     * deletes an individual reminder
     * @param reminderID reminder id
     */
    public void deleteRemindersById(String reminderID) {
        AddReminderFragment.removeAlarmFromManager(mContext, Long.valueOf(reminderID));

        SQLiteDatabase db = this.getMyWritableDatabase();
        if (db != null) {
            db.delete(ReminderTable.TABLE_NAME, ReminderTable.COLUMN_ID + " = ?",
                    new String[] { reminderID });
            //db.close();
        }
    }
}
