package com.joshafeinberg.contactreminder.databases;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ReminderTable {

    public static final String TABLE_NAME = "reminders";
    public static final String COLUMN_ID = "ReminderID";
    public static final String COLUMN_CONTACT = "ContactID";
    public static final String COLUMN_LOOKUPCONTACT = "LookupID";
    public static final String COLUMN_REMINDER = "Reminder";
    public static final String COLUMN_DUEDATE = "DueDate";

    private static final String DATABASE_CREATE =
            "create table " + TABLE_NAME + "("
                    + COLUMN_ID + " integer primary key autoincrement, "
                    + COLUMN_CONTACT + " text, "
                    + COLUMN_LOOKUPCONTACT + " text, "
                    + COLUMN_REMINDER + " text, "
                    + COLUMN_DUEDATE + " integer);";

    /**
     * creates table in database using SQLite
     * @param database database to hold reminders
     */
    public static void onCreate(SQLiteDatabase database) {
        Log.i("contactreminder", "RemindTable.onCreate");
        database.execSQL(DATABASE_CREATE);
    }

    /**
     * drops and recreates table on upgrades
     * @param database database to hold reminders
     * @param oldVersion id of old database
     * @param newVersion id of new database
     */
    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(ReminderTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }
}
